import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'simple_order_service.settings')

app = Celery('app')
app.config_from_object('django.conf:settings', namespace='CELERY')

BEAT_SCHEDULE = {
    'change_order_status_every_3_minutes': {
        'task': 'order.helpers.change_order_status',
        'schedule': 60,
    },
}

app.conf.beat_schedule = BEAT_SCHEDULE

if __name__ == '__main__':
    app.start()
