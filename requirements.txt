Django==4.2.4
djangorestframework==3.14.0
celery==5.3.1
redis==4.6.0