from datetime import timedelta

from django.test import TestCase
from django.utils import timezone
from .helpers import change_order_status
from .models import Order


class OrderModelTestCase(TestCase):

    def test_order_creation(self):
        order = Order.objects.create(status='new')
        self.assertEqual(order.status, 'new')

    def test_change_order_status_new_to_in_post_office(self):
        order_new = Order.objects.create(status='new', created_at=timezone.now() - timedelta(minutes=4))
        change_order_status()
        order_new.refresh_from_db()
        self.assertEqual(order_new.status, 'in_post_office')

    def test_change_order_status_in_post_office_to_delivered_or_rejected(self):
        order_in_post_office = Order.objects.create(
            status='in_post_office', created_at=timezone.now() - timedelta(minutes=4)
        )
        random_statuses = ['delivered', 'rejected']
        change_order_status()
        order_in_post_office.refresh_from_db()
        self.assertIn(order_in_post_office.status, random_statuses)
