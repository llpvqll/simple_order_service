from django.db import models


class Order(models.Model):
    STATUS_CHOICES = [
        ('new', 'Новий'),
        ('in_post_office', 'Розміщений у поштомат'),
        ('delivered', 'Отриманий клієнтом'),
        ('rejected', 'Відмова'),
    ]

    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='new')
    created_at = models.DateTimeField(auto_now_add=True)
