# Generated by Django 4.2.4 on 2023-08-02 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('new', 'Новий'), ('in_post_office', 'Розміщений у поштомат'), ('delivered', 'Отриманий клієнтом'), ('rejected', 'Відмова')], default='New', max_length=20)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
