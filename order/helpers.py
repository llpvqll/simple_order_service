import random
from celery_task import app
from datetime import datetime, timedelta
from .models import Order
from django.db.models import Q


@app.task()
def change_order_status():
    random_statuses = ('delivered', 'rejected')
    query = Q(created_at__lte=datetime.now() - timedelta(minutes=3))
    if orders := Order.objects.filter(query, status='new'):
        for order in orders:
            order.status = 'in_post_office'
            order.created_at = datetime.now()
            order.save()
    elif orders := Order.objects.filter(query, status='in_post_office'):
        for order in orders:
            order.status = random.choice(random_statuses)
            order.created_at = datetime.now()
            order.save()
