## For running project

* Create virtual environment:

    ``python3 -m venv .venv``

* Activate environment:

    ``source .venv/bin/activate``

* Install all dependencies:
    
    ``pip install -r requirements.txt``

* Build docker-compose image inside ``simple_order_service`` directory:

  ``docker-compose build``

* Run container:

  ``docker-compose up``