# Use an official Python runtime as a base image
FROM python:3.11

# Set environment variables for Celery and Django
ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV production

# Install system dependencies
RUN apt-get update && apt-get install -y \
    postgresql-client \
    gcc \
    libc-dev \
    && rm -rf /var/lib/apt/lists/*

# Set the working directory
WORKDIR /app

# Copy the project files to the container
COPY . /app/

# Install Python dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Expose the Django port (change if needed)
EXPOSE 8000

# Start the Django development server (modify as needed for production)
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
